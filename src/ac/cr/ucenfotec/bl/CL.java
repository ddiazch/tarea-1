package ac.cr.ucenfotec.bl;

import java.util.ArrayList;
/**
 * @author David Diaz
 * @version 1.0
 * @since 26-02-2022
 *
 * Esta clase se encarga de gestionar la capa lógica
 */

public class CL {

    static ArrayList<Cliente> listaClientes = new ArrayList<>();
    static ArrayList<Cuenta> listaCuentas = new ArrayList<>();

    /**
     * Metodo que permite crear un nuevo cliete y añadirlo a la lista Clientes
     * @param nombre es de tipo String y corresponde al nombre del cliente
     * @param identificacion es de tipo entero y corresponde a la identificación del cliente
     * @param fechaNacimiento es de tipo String y corresponde a la fecha de nacimiento del cliente
     * @param edad es de tipo entero y corresponde a la edad del cliente
     * @param direccion es de tipo String y corresponde a la dirección del domicilio del cliente
     * @return String con el resultado de la operación
     */
    public static String registrarCliente(String nombre, int identificacion, String fechaNacimiento, int edad, String direccion){

        for(Cliente clienteTemporal:listaClientes) {
            if(clienteTemporal != null){
                if(clienteTemporal.getIdentificacion() == identificacion){
                    return "El cliente con la identificación ingresada ya existe.";
                }
            }
        }

        Cliente cliente = new Cliente(nombre, identificacion, fechaNacimiento, edad, direccion);
        listaClientes.add(cliente);

        return "El cliente fue registrado exitosamente!";
    }

    /**
     * Metodo que retorna una lista de los clientes en listaCLientes
     * @return arreglo tipo String con la lista de clientes
     */
    public static String[] listarClientes(){
        String[] listaTemporalClientes = new String[listaClientes.size()];

        for (int i = 0; i < listaTemporalClientes.length; i++) {
            listaTemporalClientes[i] = listaClientes.get(i).toString();
        }
        return listaTemporalClientes;
    }

    /**
     * Metodo que permite abrir una nueva cuenta y añadirlo a la listaCuentas
     * @param numeroCuenta es de tipo entero y corresponde a la identificación del cliente
     * @param depositoInicial es de tipo double y corresponde a la cantidad inicial de saldo que se deposita en la cuenta
     * @return String con el resultado de la operación
     */
    public static String abrirCuenta(int numeroCuenta, double depositoInicial){

        if(String.valueOf(numeroCuenta).length() != 7){
            return "El número de cuenta debe tener 7 digitos";
        }
        else{
            for(Cuenta cuentaTemporal:listaCuentas) {
                if(cuentaTemporal != null){
                    if(cuentaTemporal.getNumCuenta() == numeroCuenta){
                        return "La cuenta ingresada ya existe.";
                    }
                }
            }
            if(depositoInicial >= 50000){
                Cuenta cuenta = new Cuenta(numeroCuenta, depositoInicial);
                listaCuentas.add(cuenta);

                return "La cuenta  fue abierta exitosamente.";
            }
            else{
                return "El monto inicial del depósito no puede ser menor a 50 mil colones, intente de nuevo.";
            }
        }


    }

    /**
     * Metodo que permite realizar un deposito a la cuenta especificada
     * @param numeroCuenta es de tipo entero y corresponde a la identificación del cliente
     * @param monto es de tipo double y corresponde a la cantidad que se deposita en la cuenta
     * @return String con el resultado de la operación
     */
    public static String realizarDeposito(int numeroCuenta, double monto){

        for(Cuenta cuentaReferencia:listaCuentas) {
            if(cuentaReferencia.getNumCuenta() == numeroCuenta){
                if(monto <= 0){
                    return "No se pueden depositar montos en negativo o sin valor.";
                }
                else{
                    cuentaReferencia.depositar(monto);
                    return "Se depositaron " + monto + " colones en la cuenta.";
                }
            }
        }
        return "La cuenta ingresada no existe.";
    }

    /**
     * Metodo que permite realizar un retiro a la cuenta especificada
     * @param numeroCuenta es de tipo entero y corresponde a la identificación del cliente
     * @param monto es de tipo double y corresponde a la cantidad que se retira de la cuenta
     * @return String con el resultado de la operación
     */
    public static String realizarRetiro(int numeroCuenta, double monto){

        for(Cuenta cuentaReferencia:listaCuentas) {
            if(cuentaReferencia.getNumCuenta() == numeroCuenta){
                if(monto <= 0){
                    return "No se pueden retirar montos en negativo o sin valor.";
                }
                else if(monto > cuentaReferencia.getSaldo()){
                    return "No se pueden retirar montos mayores al saldo disponible en la cuenta.";
                }
                else{
                    cuentaReferencia.retirar(monto);
                    return "Se depositaron " + monto + " colones en la cuenta.";
                }
            }
        }
        return "La cuenta ingresada no existe.";
    }

    /**
     * Metodo que retorna el saldo disponible de la cuenta especificada
     * @param numeroCuenta es de tipo entero y corresponde a la identificación del cliente
     * @return String con el saldo disponible o el resultado de la operación
     */
    public static String mostrarSaldo(int numeroCuenta){

        for(Cuenta cuentaReferencia:listaCuentas) {
            if(cuentaReferencia.getNumCuenta() == numeroCuenta){
                double saldo = cuentaReferencia.getSaldo();
                if (saldo == 0){
                    return "ac.cr.ucenfotec.bl.Cuenta no tiene fondos.";
                }else{
                    return "El saldo de la cuenta es de " + saldo + " colones.";
                }
            }
        }
        return "La cuenta ingresada no existe.";
    }

}

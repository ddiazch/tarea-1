package ac.cr.ucenfotec.bl;

/**
 * @author David Diaz
 * @version 1.0
 * @since 26-02-2022
 *
 * Esta clase se encarga de gestionar todos los objetos cliente
 */

public class Cliente {

    //atributos
    private  String nombre;
    private int identificacion;
    private String fechaNacimiento;
    private int edad;
    private String direccion;

    /**
     * Constructor que crea un cliente con la informacion proveida
     * @param nombre es de tipo String y corresponde al nombre del cliente
     * @param identificacion es de tipo entero y corresponde a la identificación del cliente
     * @param fechaNacimiento es de tipo String y corresponde a la fecha de nacimiento del cliente
     * @param edad es de tipo entero y corresponde a la edad del cliente
     * @param direccion es de tipo String y corresponde a la dirección del domicilio del cliente
     */
    public Cliente(String nombre, int identificacion, String fechaNacimiento, int edad, String direccion){
        this.nombre = nombre;
        this.identificacion = identificacion;
        this.fechaNacimiento = fechaNacimiento;
        this.edad = edad;
        this.direccion = direccion;
    }

    /**
     * Metodo que devuelve la identificación
     * @return entero con la identificación del objeto
     */
    public int getIdentificacion() {
        return identificacion;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public void setIdentificacion(int identificacion) {
        this.identificacion = identificacion;
    }

    public String getFechaNacimiento() {
        return fechaNacimiento;
    }

    public void setFechaNacimiento(String fechaNacimiento) {
        this.fechaNacimiento = fechaNacimiento;
    }

    public int getEdad() {
        return edad;
    }

    public void setEdad(int edad) {
        this.edad = edad;
    }

    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    /**
     * Metodo que devuelve los atributos en formato String
     * @return String con el estado del objeto
     */
    public String toString() {
        return "nombre:" + nombre +
                ", identificación:" + identificacion +
                ", fecha de nacimiento:" + fechaNacimiento +
                ", edad=" + edad +
                ", dirección=" + direccion;
    }
}

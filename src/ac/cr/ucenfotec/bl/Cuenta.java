package ac.cr.ucenfotec.bl;

/**
 * @author David Diaz
 * @version 1.0
 * @since 26-02-2022
 *
 * Esta clase se encarga de gestionar todos los objetos cuenta
 */

public class Cuenta {

    private int numeroCuenta;
    private double saldo;
    private Cliente duenno;

    /**
     * Constructor que crea una cuenta con la informacion proveida
     * @param numeroCuenta es de tipo entero y corresponde a la identificación del cliente
     * @param depositoInicial es de tipo double y corresponde a la cantidad inicial de saldo que se deposita en la cuenta
     */
    public Cuenta(int numeroCuenta, double depositoInicial){
        this.numeroCuenta = numeroCuenta;
        this.saldo = depositoInicial;
    }

    /**
     * Metodo que devuelve el numero de cuenta
     * @return entero con el numero de cuenta
     */
    public int getNumCuenta() {
        return numeroCuenta;
    }


    //get y set
    public int getNumeroCuenta() {
        return numeroCuenta;
    }

    public void setNumeroCuenta(int numeroCuenta) {
        this.numeroCuenta = numeroCuenta;
    }

    public double getSaldo() {
        return saldo;
    }

    public void setSaldo(double saldo) {
        this.saldo = saldo;
    }

    public Cliente getDuenno() {
        return duenno;
    }

    public void setDuenno(Cliente duenno) {
        this.duenno = duenno;
    }

    /**
     * Metodo que devuelve el saldo disponible en la cuenta
     * @return double con el saldo disponible en la cuenta
     */


    public void depositar(double monto){
        this.saldo = this.saldo + monto;
    }

    public void retirar(double monto){
        this.saldo = this.saldo - monto;
    }


}
